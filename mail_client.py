import imaplib
import time
from imap_tools import MailBox, Q
from decouple import config
from getpass import getpass

hostname = config("hostname")
username = config("username")
password = getpass("password for {}?".format(username))


def get_unseen_emails() :
    with MailBox(hostname).login(username, password) as mailbox:
        new_mails = []
        try :
            new_mails = [msg for msg in mailbox.fetch(Q(seen=False), mark_seen=False)]
        except imaplib.abort:
            pass
        return new_mails

