import RPi.GPIO as GPIO
from time import sleep
class Diode :
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(18,GPIO.OUT)
        GPIO.output(18, GPIO.LOW)

    def turn_on(self):
        GPIO.output(18, GPIO.HIGH)
    
    def turn_off(self):
        GPIO.output(18, GPIO.LOW)


