import time

from diode import Diode
from mail_client import get_unseen_emails

diode = Diode()

while True :
    try :
        print("waiting for new emails...")
        unseen = get_unseen_emails()
        l = len(unseen)
        if len(unseen) :
            print("{} new email(s)".format(l))
            diode.turn_on()
        else :
            diode.turn_off()
        time.sleep(10)
    except KeyboardInterrupt :
        break
    except :
        pass

